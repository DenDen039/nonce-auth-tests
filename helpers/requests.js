require('dotenv').config();

async function PostData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'POST',
        body: JSON.stringify(data)
    });
    return await response.json();
}

function CreateNonceRequest(public_key, terms_msg = '') {
    let req = {
        "data": {
            "type": "auth_nonce_request",
            "attributes": {
                "address": public_key,
                "terms": terms_msg
            }
        }
    }
    return req
}
function CreateRegisterRequest(public_key, sig_message) {
    return {
        data: {
            type: "register",
            attributes: {
                auth_pair: {
                    address: public_key,
                    signed_message: sig_message
                }
            }
        }
    }
}
function CreateLoginRequest(public_key, sig_message) {
    return {
        data: {
            type: "login_request",
            attributes: {
                auth_pair: {
                    "address": public_key,
                    "signed_message": sig_message
                }
            }
        }
    }
}
function ExtractNonce(response) {
    return response.data.attributes.message.split('\n')[3]
}

module.exports = { PostData, CreateNonceRequest, ExtractNonce, CreateRegisterRequest, CreateLoginRequest };
global.server = process.env.API_URL;