let Web3 = require('web3');
let web3 = new Web3();

function GenerateKeypair() {
    return web3.eth.accounts.create();
}
function GeneratePublickey() {
    let account = GenerateKeypair();
    return account.address;
}
module.exports = { GenerateKeypair, GeneratePublickey };