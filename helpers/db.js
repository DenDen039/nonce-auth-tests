require('dotenv').config();
const { Pool } = require('pg');
const conn = new Pool({ connectionString: process.env.DATABASE_URL })

async function WipeDb(table_name) {
    let query = 'DELETE FROM ' + table_name + ';';
    const db = await conn.connect();
    await db.query(query);
    db.release()
}

module.exports = { WipeDb };