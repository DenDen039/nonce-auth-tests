process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

let db = require('../helpers/db');
let requests = require('../helpers/requests');
let crypto = require('../helpers/crypto')

let signed_message;
let nonce;
let keypair;

chai.use(chaiHttp);

describe('Registration', () => {

    beforeEach(async (done) => {
        try {
            let nonce_request = requests.CreateNonceRequest(keypair.address)
            let response = await requests.PostData("http://" + server + "/nonce", nonce_request);
            nonce = requests.ExtractNonce(response)
            signed_message = keypair.sign(nonce).signature
        }
        catch (e) {
            console.log("Error occurred")
            console.log(e)
        }
        done();
    });

    before(async (done) => {
        try {
            await db.WipeDb("users")
        }
        catch (e) {
            console.log("Error occurred")
            console.log(e)
        }
        keypair = crypto.GenerateKeypair()

        done();
    });

    describe('/POST register an address', () => {
        it('It should return account data value message', async (done) => {
            let register_request = requests.CreateRegisterRequest(keypair.address, signed_message)

            chai.request(server)
                .post('/register')
                .send(register_request)
                .end((err, res) => {
                    res.should.have.status(201);
                    done();
                });
        });
    });
    describe('/POST register the same address', () => {
        it('It should return conflict response', (done) => {
            let register_request = requests.CreateRegisterRequest(keypair.address, signed_message)

            chai.request(server)
                .post('/register')
                .send(register_request)
                .end((err, res) => {
                    res.should.have.status(409);
                    done();
                });
        });
    });
    describe('/POST try to make another reuquest with same nonce', () => {
        it('It should return bad request response', async (done) => {
            try {
                await db.WipeDb("users")
            }
            catch (e) {
                console.log("Error occurred")
                console.log(e)
            }
            let register_request = requests.CreateRegisterRequest(keypair.address, signed_message)
            await requests.PostData("http://" + server + "/register", register_request)
            try {
                await db.WipeDb("users")
            }
            catch (e) {
                console.log("Error occurred")
                console.log(e)
            }
            chai.request(server)
                .post('/register')
                .send(register_request)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
    });
    describe('/POST register a wrong address', () => {
        it('It should return bad request response', async (done) => {
            try {
                await db.WipeDb("users")
            }
            catch (e) {
                console.log("Error occurred")
                console.log(e)
            }
            let register_request = requests.CreateRegisterRequest(crypto.GeneratePublickey, signed_message)

            chai.request(server)
                .post('/register')
                .send(register_request)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
    });

    describe('/POST register a wrong sign message', () => {
        it('It should return bad request response', async (done) => {
            try {
                await db.WipeDb("users")
            }
            catch (e) {
                console.log("Error occurred")
                console.log(e)
            }
            let fake_signature = crypto.GenerateKeypair().sign(nonce).signature
            let register_request = requests.CreateRegisterRequest(keypair.address, fake_signature)

            chai.request(server)
                .post('/register')
                .send(register_request)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
    });

});