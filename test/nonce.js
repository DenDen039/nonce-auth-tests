process.env.NODE_ENV = 'test';


let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
let db = require('../helpers/db')
let requests = require('../helpers/requests')
let crypto = require('../helpers/crypto')


chai.use(chaiHttp);


describe('Nonces', () => {
  beforeEach(async (done) => {
    try {
      await db.WipeDb('nonce');
    }
    catch (e) {
      console.log("Error occurred")
      console.log(e)
    }
    done();
  });

  describe('/POST nonce with terms', () => {
    it('It should return nonce value message', (done) => {
      let nonce_request = requests.CreateNonceRequest(crypto.GeneratePublickey(), 'test')
      chai.request(server)
        .post('/nonce')
        .send(nonce_request)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('data');
          res.body.data.should.have.property('attributes')
          res.body.data.attributes.should.have.property('message')
          done();
        });
    });

  });
  describe('/POST nonce without terms', () => {
    it('It should return nonce value message', (done) => {
      let nonce_request = requests.CreateNonceRequest(crypto.GeneratePublickey())
      chai.request(server)
        .post('/nonce')
        .send(nonce_request)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('data');
          res.body.data.should.have.property('attributes')
          res.body.data.attributes.should.have.property('message')
          res.body.data.attributes.message.length.should.be.eql(130);
          done();
        });
    });
  });
  describe('/POST nonce with incorrect address', () => {
    it('It return bad request', (done) => {
      let nonce_request = requests.CreateNonceRequest('0x000')
      chai.request(server)
        .post('/nonce')
        .send(nonce_request)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});