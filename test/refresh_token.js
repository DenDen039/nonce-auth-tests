process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');

let db = require('../helpers/db');
let requests = require('../helpers/requests');
let crypto = require('../helpers/crypto')

let signed_message;
let nonce;
let keypair;
let refresh_token;

chai.use(chaiHttp);

describe('Refresh_token', () => {

    beforeEach(async (done) => {
        try {
            await db.WipeDb("users")

            let nonce_request = requests.CreateNonceRequest(keypair.address)
            let response = await requests.PostData("http://" + server + "/nonce", nonce_request);
            nonce = requests.ExtractNonce(response)
            signed_message = keypair.sign(nonce).signature;

            let register_request = requests.CreateRegisterRequest(keypair.address, signed_message)

            let account_data = await requests.PostData("http://" + server + "/register", register_request)
            refresh_token = account_data.data.attributes.refresh_token
        }
        catch (e) {
            console.log("Error occurred")
            console.log(e)
        }
        done();
    });

    before(async (done) => {
        keypair = crypto.GenerateKeypair()
        done();
    });


    describe('/POST refresh token', () => {
        it('It should return get new access and refresh token', (done) => {
            let refresh_request = {
                "refresh_token": refresh_token
            }

            chai.request(server)
                .post('/refresh_token')
                .set({ "Authorization": `Bearer ${refresh_token}` })
                .send(refresh_request)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });

    describe('/POST not correct refresh token', () => {
        it('It should return bad request error', (done) => {
            let refresh_request = {
                "refresh_token": refresh_token + 'a'
            }

            chai.request(server)
                .post('/refresh_token')
                .set({ "Authorization": `Bearer ${refresh_token}` })
                .send(refresh_request)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
    });

    describe('/POST not correct authorization header', () => {
        it('It should return unauthorized request error', (done) => {
            let refresh_request = {
                "refresh_token": refresh_token
            }

            chai.request(server)
                .post('/refresh_token')
                .set({ "Authorization": `Bearer ${refresh_token + 'a'}` })
                .send(refresh_request)
                .end((err, res) => {
                    res.should.have.status(401);
                    done();
                });
        });
    });

});