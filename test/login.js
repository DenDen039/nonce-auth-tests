process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');

let db = require('../helpers/db');
let requests = require('../helpers/requests');
let crypto = require('../helpers/crypto')

let signed_message;
let nonce;
let keypair;

chai.use(chaiHttp);

describe('Login', () => {
    beforeEach(async (done) => {
        try {
            let nonce_request = requests.CreateNonceRequest(keypair.address)
            let response = await requests.PostData("http://" + server + "/nonce", nonce_request);
            nonce = requests.ExtractNonce(response)
            signed_message = keypair.sign(nonce).signature
        }
        catch (e) {
            console.log("Error occurred")
            console.log(e)
        }
        done();
    });
    before(async (done) => {
        try {
            await db.WipeDb("users")

            keypair = crypto.GenerateKeypair()

            let nonce_request = requests.CreateNonceRequest(keypair.address)
            let response = await requests.PostData("http://" + server + "/nonce", nonce_request);
            nonce = requests.ExtractNonce(response)
            signed_message = keypair.sign(nonce).signature

            let register_request = requests.CreateRegisterRequest(keypair.address, signed_message)

            await requests.PostData("http://" + server + "/register", register_request)

            nonce_request = requests.CreateNonceRequest(keypair.address)
            response = await requests.PostData("http://" + server + "/nonce", nonce_request);
            nonce = requests.ExtractNonce(response)
            signed_message = keypair.sign(nonce).signature
        }
        catch (e) {
            console.log("Error occurred")
            console.log(e)
        }
        done();
    });

    describe('/POST login to account', () => {
        it('It should return account data value message', (done) => {
            let login_request = requests.CreateLoginRequest(keypair.address, signed_message)
            chai.request(server)
                .post('/login')
                .send(login_request)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });

    });
    describe('/POST login with used nonce', () => {
        it('It should return bad request', async (done) => {
            let login_request = requests.CreateLoginRequest(keypair.address, signed_message)
            await requests.PostData("http://" + server + "/login",login_request)
            chai.request(server)
                .post('/login')
                .send(login_request)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });

    });

    describe('/POST login to account with wrong address', () => {
        it('It should return bad request', async (done) => {
            let login_request = requests.CreateLoginRequest(crypto.GeneratePublickey(), signed_message)
            chai.request(server)
                .post('/login')
                .send(login_request)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });

    });
    describe('/POST login to account with wrong signature', () => {
        it('It should return bad request', (done) => {
            let fake_signature = crypto.GenerateKeypair().sign(nonce).signature
            let login_request = requests.CreateLoginRequest(keypair, fake_signature)

            chai.request(server)
                .post('/login')
                .send(login_request)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });

    });

});